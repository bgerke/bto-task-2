from .. import aggregation as agg
from ..utils.dates import create_cal_data
import numpy as np
import pandas as pd

class TestAvgLoadshape:
    '''
    Tests for agg.compute_average_loadshape
    '''
    @classmethod
    def setup_class(self):
        #Create a test dataset for four months
        data = create_cal_data('01-01-2018', '05-01-2018')
        data['Load'] = 0
        data['Load'] = data['month'].values 
        #Each month has flat load, increasing by month
        self.data=data
        
    def test_compute_avg_basic(self):
        '''
        Running on self.data should be a 24-hour load shape with the 
        same flat value in each month
        '''
        aggls = agg.compute_average_loadshapes(self.data)
        
        for m in self.data['month'].unique():
            #Should have a single-day load shape for each month
            assert ((aggls['month']==m).sum() == 24)
            #Should be a flat load shape with value m
            assert (aggls.loc[aggls['month']==m, 'Load']==m).all()
            
    def test_is_averaging_by_hour(self):
        '''
        compute_average_loadshapes should be averaging by hour in the
        default case
        '''
        data = self.data.copy()
        
        #Create random data in one hour of each day
        data.loc[data['dhour']==12, 'Load'] = np.random.random_sample(
                                                    (data['dhour']==12).sum())
        
        aggls = agg.compute_average_loadshapes(data)
        
        for m in self.data['month'].unique():
            sel = (data['month']==m) & (data['dhour']==12)
            #Hour 12 should be an average of the random data
            assert aggls.loc[(aggls['month']==m) & (aggls['dhour']==12), 
                             'Load'].values[0] == data.loc[sel, 'Load'].mean()
            #All other hours should be as before
            assert (aggls.loc[(aggls['month']==m) & (aggls['dhour']!=12), 
                              'Load'] == m).all()
            #And these two should be different
            assert data.loc[sel, 'Load'].mean() != m
            
    def test_by_weekday_weekend(self):
        '''
        If we also group by weekday/weekend, should yield two days per month
        '''
        
        data = self.data.copy()
        
        #all weekends have zero load
        data.loc[data['weekend']==1, 'Load'] = 0
        
        aggls = agg.compute_average_loadshapes(data, by=['month','weekend'])
        
        for m in self.data['month'].unique():
            
            #Should have two days per month
            assert len(aggls.loc[aggls['month']==m, 'Load'])==48
            #Weekdays should have values as before
            assert (aggls.loc[(aggls['month']==m) & (aggls['weekend']==0), 
                              'Load'] == m).all()
            #Weekends should all have average value 0
            assert (aggls.loc[(aggls['month']==m) & (aggls['weekend']==1), 
                              'Load'] == 0).all()
            
    def test_output_cols(self):
        '''
        Output dataframe should have the columns that were grouped by
        '''
        
        aggls = agg.compute_average_loadshapes(self.data)
        
        for c in aggls.columns:
            assert c in ['month', 'dhour', 'Load']
            
        aggls = agg.compute_average_loadshapes(self.data, by=['month', 'weekend'])
        
        for c in aggls.columns:
            assert c in ['month', 'weekend', 'dhour', 'Load']
            

class TestExpandAvg:
    '''
    tests for expand_average_loadshapes
    '''
    
    def test_expand_one_day(self):
        avgdat = pd.DataFrame()
        avgdat['dhour'] = np.arange(24)
        avgdat['Load'] = np.random.rand(24)
        caldat = create_cal_data('01-01-2018', '03-01-2018')
        
        expdat = agg.expand_average_loadshapes(avgdat, caldat)
        
        for h in avgdat['dhour']:
            assert (expdat.loc[expdat['dhour']==h, 'Load'] == 
                    avgdat.loc[avgdat['dhour']==h, 'Load'].values[0]).all()
            
    def test_expand_multi_day(self):
        '''
        Should also expand out correctly if we have multiple day types
        '''
        avgdat = pd.DataFrame()
        avgdat['dhour'] = np.arange(24)
        avgdat['month'] = 1
        avgdat2 = avgdat.copy()
        avgdat2['month'] = 2
        avgdat = pd.concat([avgdat, avgdat2])
        avgdat['Load'] = np.random.rand(48)
        caldat = create_cal_data('01-01-2018', '03-01-2018')
        
        expdat = agg.expand_average_loadshapes(avgdat, caldat)
        
        for m in avgdat['month'].unique():
            for h in avgdat['dhour'].unique():
                assert (expdat.loc[(expdat['month']==m) & (expdat['dhour']==h), 
                                   'Load'] == 
                        avgdat.loc[(avgdat['month']==m) & (avgdat['dhour']==h), 
                                   'Load'].values[0]).all()