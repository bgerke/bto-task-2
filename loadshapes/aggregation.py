

def compute_average_loadshapes(data, by=['month'], 
                               cols=['Load'], avg_time_col='dhour'):
    '''
    Given a dataset containing interval load data and calendar information, 
    return a dataset containing average load shapes, by specified time
    periods.  
    
    Ultimately this is a thin wrapper around pandas groupby, with some
    convenient syntax conventions
    
    Inputs
    ------
    data: A dataframe containing load shape data in one or more columns
        and calendar information in other columns
        
    Keywords:
    ---------
    by: A list of the columns for which we want an average load shape. 
        For instance: ['month', 'weekend'] would produce an average load 
        shape for weekdays and weekend days in each month.
    avg_time_col: The column to use as the time variable in the averaged 
        load shapes. For instance 'dhour' would yield the average load by
        hour of day--thus producing a daily load shape.
    cols: A list of columns containing the data to be averaged.
    
    '''
    if type(by) == type(''): by=[by]
    if type(cols) == type(''): cols=[cols]
    return data.groupby(by+[avg_time_col])[cols].mean().reset_index()


def expand_average_loadshapes(avgdata, caldata):
    '''
    Given a dataset containing average load shapes for various
    time periods (e.g., month, day type, etc), along with a data
    frame containing calendar information for a broader time frame 
    (e.g., all 8760 hours of the year), expand the average load shapes
    by mapping them onto the provided calendar. This is a means of,
    for instance, expanding a set of average monthly load shapes into
    an 8760 load shape.
    
    This function is a *very* thin wrapper around pandas.DataFrame.merge().
    Its only reason for existing is to eliminate the need to remember
    the required syntax.  
    
    This function merges the two input data frames on their shared
    columns. The user must ensure that the input data frames share
    the correct columns (time periods and time variable) and no others.
    In particular, the caldata input should NOT contain any load data.
    
    Inputs:
    -------
    avgdata: A data frame containing average load shapes for a given
        set of time periods. Must include coluns specifying the time
        periods, a column specifying the time variable, and one or 
        more columns containing load or savings shape information.
        
    caldata: A data frame containing calendar information. It should contain
        the same time period and time variable columns as the avgdata 
        data frame. It must NOT contain any of the load columns from the
        avgdata dataframe.
    '''

    return caldata.merge(avgdata, how='left')

