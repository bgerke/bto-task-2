from .. import dates
import pandas as pd

def test_cal_data_has_expected_columns():
    '''
    Check that create_cal_data yields the expected columns
    '''
    caldat=dates.create_cal_data('01-01-2018','01-02-2018')
    expected_cols = [
        'dow','dom','doy','dhour','week','weekend','season','quarter']
    for ec in expected_cols:
        assert ec in caldat.columns
        
def test_cal_data_has_expected_length():
    '''
    test that create_cal_data gives the expected number of rows
    for a given frequency
    '''
    #One day, hourly frequency
    assert len(dates.create_cal_data('01-01-2018','01-02-2018'))==24
               
    #One day, minute frequency
    assert len(dates.create_cal_data('01-01-2018','01-02-2018',
                                     frequency='T'))==24*60
               
    #One year, hourly frequency
    assert len(dates.create_cal_data('01-01-2018','01-01-2019',
                                     frequency='H'))==8760
    
    #Leap year, hourly frequency
    assert len(dates.create_cal_data('01-01-2016','01-01-2017',
                                     frequency='H'))==8760+24
    

class TestPeakFinding:
    '''Test for the peak-finding function'''
    
    @classmethod
    def setup_class(self):
        #Create a simple dataset for a four-month period
        data = dates.create_cal_data('01-01-2006', '05-01-2006')
        data['Load'] = 0
        
        #Create a peak hour on the fifth of each month at noon
        data.loc[(data['dom']==5) & (data['dhour']==12), 'Load']=1
        
        #Create a peak consumption day on the sixth of each month
        data.loc[(data['dom']==6), 'Load']=0.5
        
        #Create a third-highest peak load and consumption day on the 15th
        data.loc[(data['dom']==15), 'Load']=0.25
        
        self.data=data
        
    def test_flag_peak_load_month(self):
        '''Test that we correctly flag peak load days by month'''
        
        #For a single peak day, flag only the fifth
        peakflag = dates.get_peak_days(self.data)
        assert (peakflag[self.data['dom']==5]==1).all()
        assert (peakflag[self.data['dom']!=5]==0).all()
        
        #For two peak days, flag the 5th and 6th
        peakflag = dates.get_peak_days(self.data, npeaks=2)
        assert (peakflag[self.data['dom'].isin([5,6])]==1).all()
        assert (peakflag[~self.data['dom'].isin([5,6])]==0).all()
        
        
        #For three peak days, flag the 5th, 6th, and 15th
        peakflag = dates.get_peak_days(self.data, npeaks=3)
        assert (peakflag[self.data['dom'].isin([5,6,15])]==1).all()
        assert (peakflag[~self.data['dom'].isin([5,6,15])]==0).all()
        
    def test_flag_peak_consumption_month(self):
        '''Test that we correctly flag peak consumption days by month'''
        
        #For a single peak day, flag only the sixth
        peakflag = dates.get_peak_days(self.data, peak_metric='consumption')
        assert (peakflag[self.data['dom']==6]==1).all()
        assert (peakflag[self.data['dom']!=6]==0).all()
        
        #For two peak days, flag the 6th and 15th
        peakflag = dates.get_peak_days(self.data, npeaks=2, 
                                       peak_metric='consumption')
        assert (peakflag[self.data['dom'].isin([15,6])]==1).all()
        assert (peakflag[~self.data['dom'].isin([15,6])]==0).all()
        
        
        #For three peak days, flag the 5th, 6th, and 15th
        peakflag = dates.get_peak_days(self.data, npeaks=3,
                                      peak_metric='consumption')
        assert (peakflag[self.data['dom'].isin([5,6,15])]==1).all()
        assert (peakflag[~self.data['dom'].isin([5,6,15])]==0).all()
        

    def test_flag_peak_ties(self):
        '''
        Test that the peak day finding includes all ties
        '''
        
        data = self.data.copy()
        
        #Create a tie second-place peak load day
        data.loc[(data['dom']==10) & (data['dhour']==12), 'Load']=0.5
        
        #Create a tie second-place peak consumption day 
        data.loc[(data['dom']==20),
                 'Load']=0.25
        
        #For peak load, with npeaks=2 flag the 5th and 6th, leaving out the 10th 
        #(this is prioritizing the first tie)
        peakflag = dates.get_peak_days(data, npeaks=2,
                                      peak_metric='load')
        assert (peakflag[data['dom'].isin([5,6])]==1).all()
        assert (peakflag[~data['dom'].isin([5,6])]==0).all()
        #Get the top 3 for npeaks=3, including the second-place tie, but 
        #not including the third-place day excluding ties
        peakflag = dates.get_peak_days(data, npeaks=3,
                                      peak_metric='load')
        assert (peakflag[data['dom'].isin([5,10,6])]==1).all()
        assert (peakflag[~data['dom'].isin([5,10,6])]==0).all()
        #Need to go to npeaks=4 to get the 15th, but not the 20th
        peakflag = dates.get_peak_days(data, npeaks=4,
                                      peak_metric='load')
        assert (peakflag[data['dom'].isin([5,10,6,15])]==1).all()
        assert (peakflag[~data['dom'].isin([5,10,6,15])]==0).all()
        
        #Similar tests as above for consumption
        peakflag = dates.get_peak_days(data, npeaks=2,
                                      peak_metric='consumption')
        assert (peakflag[data['dom'].isin([6,15])]==1).all()
        assert (peakflag[~data['dom'].isin([6,15])]==0).all()
        peakflag = dates.get_peak_days(data, npeaks=3,
                                      peak_metric='consumption')
        assert (peakflag[data['dom'].isin([6,15,20])]==1).all()
        assert (peakflag[~data['dom'].isin([6,15,20])]==0).all()
        peakflag = dates.get_peak_days(data, npeaks=4,
                                      peak_metric='consumption')
        assert (peakflag[data['dom'].isin([5,6,15,20])]==1).all()
        assert (peakflag[~data['dom'].isin([5,6,15,20])]==0).all()

        
    def test_flat_loadshape_peak_finding(self):
        '''
        If the load shape is flat, the peak finding algorithm should
        just pick the first npeaks days. Peak days are identical to 
        non-peak days in this case.
        '''
        
        data=self.data.copy()
        data['Load'] = 1.
        
        #npeaks=1 should get day 1
        peakflag = dates.get_peak_days(data, npeaks=1,
                                      peak_metric='load')
        assert (peakflag[data['dom'].isin([1])]==1).all()
        assert (peakflag[~data['dom'].isin([1])]==0).all()
        
        #npeaks=2 should get first two days
        peakflag = dates.get_peak_days(data, npeaks=2,
                                      peak_metric='load')
        assert (peakflag[data['dom'].isin([1,2])]==1).all()
        assert (peakflag[~data['dom'].isin([1,2])]==0).all()
        
        #repeat for consumption
        peakflag = dates.get_peak_days(data, npeaks=1,
                                      peak_metric='consumption')
        assert (peakflag[data['dom'].isin([1])]==1).all()
        assert (peakflag[~data['dom'].isin([1])]==0).all()
        
        peakflag = dates.get_peak_days(data, npeaks=2,
                                      peak_metric='consumption')
        assert (peakflag[data['dom'].isin([1,2])]==1).all()
        assert (peakflag[~data['dom'].isin([1,2])]==0).all()
        
    def test_multiple_scenarios(self):
        '''
        Should be able to correctly flag peak days even if the day-of-year
        field is not unique (e.g., if the data contains multiple scenarios)
        '''
        data=self.data.copy()
        data['Scenario']='A'
        data2 = self.data.copy()
        data2['Scenario']='B'
        
        #Create a different peak load day in the second scenario
        data2.loc[(data2['dom']==10) & (data2['dhour']==12), 'Load']=2.
        
        data = pd.concat([data, data2], ignore_index=True)
        
        peakflag = dates.get_peak_days(data, by=['Scenario', 'month'])
        
        #Should only be one peak day in each month.
        assert (peakflag.sum()==2*4*24)
        
        #Should find the right peak day in each scenario
        assert (data.loc[(peakflag==1) & (data['Scenario']=='A'), 'dom']==5).all()
        assert (data.loc[(peakflag==1) & (data['Scenario']=='B'), 'dom']==10).all()
        
        
def test_generate_day_types():
    '''
    Check that day types are correctly generated
    '''
    
    data = dates.create_cal_data('11-01-2019', '12-01-2019')
    
    data['Load'] = 0
    
    #Create peak days on Tuesday the 5th and Saturday the 9th
    data.loc[((data['dom'].isin([5,9])) & (data['dhour']==12)), 'Load'] = 1
    
    data = dates.generate_day_types(data, npeaks=1)
        
    #Check that day types are as expected for just the first peak
    assert (data.loc[data['dom'].isin([5]), 'Day Type']==1).all()
    assert (data.loc[(~data['dom'].isin([5]) & (data['weekend']==0)), 
                     'Day Type']==2).all()
    assert (data.loc[(~data['dom'].isin([5]) & (data['weekend']==1)), 
                     'Day Type']==3).all()
    
    data = dates.generate_day_types(data, npeaks=2)
    #Now check for both peaks
    assert (data.loc[data['dom'].isin([5,9]), 'Day Type']==1).all()
    assert (data.loc[(~data['dom'].isin([5,9]) & (data['weekend']==0)), 
                     'Day Type']==2).all()
    assert (data.loc[(~data['dom'].isin([5,9]) & (data['weekend']==1)), 
                     'Day Type']==3).all()