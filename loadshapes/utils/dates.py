import numpy as np
import pandas as pd


def create_cal_data(start_date='01-01-2006', 
                    end_date='01-01-2007',
                    frequency='H', timestamps=None):
    '''
    This function creates a calendar data table starting on 
    start_date and ending on the day BEFORE end_date.

    The table has the following columns baked-in:
    seasons, hour, day of the week, day, week, month, quarter, 
    season, weekend.
    
    In this function "season" is mapped to three-month periods, 
    where "spring" begins on March 1. It is not based on equinox/
    solstice seasons. The seasons are coded with winter=1 and 
    autumn=4
    
    The day of week is coded as in pandas.Series.dt.dayofweek, with
    Monday=0 and Sunday=6. The "week" field represents the week
    of the year.

    Arguments:
    ----------
    start_date: format is 'MM-DD-YYYY', e.g. '01-01-2018'
    
    end_date: format is 'MM-DD-YYYY', e.g. '01-02-2018'
    
    Keywords:
    ---------
    frequency: Frequency of the data (hours, minutes, seconds, etc.). 
        Format is the same as for the pandas date_range freq keyword, 
        e.g. 'H' will create hourly values; details are documented at
        https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#dateoffset-objects
    
    timestamps: If desired, can pass in an explicit pandas Series of 
        timestamps (i.e., datetime objects) for which to compute calendar 
        information. If this argument is included, start_date, end_date, 
        and frequency are ignored.

    Returns a data frame containing calendar data for the input date
    range and frequency.
    '''

    seasons = np.array([1,2,3,4])
    if timestamps is None:
        timestamps = pd.date_range(
            start=start_date, end=end_date, freq=frequency)[:-1]
    caldat = pd.DataFrame(timestamps, columns=['timestamp'])
    caldat['dow'] = caldat['timestamp'].dt.dayofweek
    caldat['dom'] = caldat['timestamp'].dt.day
    caldat['doy'] = caldat['timestamp'].dt.dayofyear
    caldat['week'] = caldat['timestamp'].dt.week
    caldat['month'] = caldat['timestamp'].dt.month
    caldat['dhour'] = caldat['timestamp'].dt.hour
    caldat['quarter'] = caldat['timestamp'].dt.quarter
    caldat['season'] = seasons[(caldat['timestamp'].dt.month // 3) % 4]
    caldat['weekend'] = (caldat['dow']>4).astype(int)
    caldat['yhour'] = (caldat['doy']-1)*24 + caldat['dhour']

    return caldat



def get_peak_days(data, by=['month'], npeaks=1, loadcol='Load', 
                  peak_metric='load', daycol='doy'):
    '''
    Given an input data frame containing hourly load data,
    flag the N peak days within each specified time period. For
    instance, we may pick the three peak days in each month, or
    the one peak day in each season, or any other interesting combination.
    The input dataset must contain a column for the time period within
    which the peaks are being found (i.e., month or season in the above
    examples).
    
    In the case of ties (more than one peak day with the same value), the
    function flags all of the relevant days. So if npeaks=3, but there are
    two days that tie for third place, then we will flag four peak days.
    
    Arguments:
    ----------
    data: a data frame containing load data, as well as columns indicating
          the day of the year and the period within which peaks are to be
          found
          
    Keywords:
    ---------
    by: the column on which to group the data when finding peaks 
        (e.g. 'month'), if we wish to find the peak day(s) in each month.
        
    npeaks: the number of peak days to flag in each period. The function
        selects the days in each period that have the highest npeaks 
        peak_metric values.
        
    loadcol: the name of the column containing the load data.
    
    daycol: the name of the column that encodes unique day information. 
        (If create_cal_data is used to generate calendar information, then
        the default value 'doy' is correct.)
    
    peak_metric: The metric to use for finding peak days. Can take the values
        'load' or 'consumption'. If 'load', then the day(s) containing the
        highest peak value(s) within each period are selected. If 'consumption',
        then the day(s) with the highest total consumption in each period
        are selected. Any values other than 'load' or 'consumption' will default
        to the 'load' approach.
        
    Returns a series of ones and zeros with identical index to the input
    data, indicating the rows that represent peak days.
    '''
    
    if type(by) == type(''): by=[by]
    
    #Get a DataFrame containing the period, day of year, and daily peak values
    #for the npeaks highest peak days in each period
    #The complicated groupby statements below first group by period and day, 
    #then find the max load (or the total consumption) in each day, then
    #group by period and find the largest npeaks days in each period, then
    #reset index to make the day index into a column.
    if peak_metric=='consumption':
        #Find the days with the largest SUM of hourly load
        daily_vals = data.groupby(
                        by+[daycol])[loadcol].sum().reset_index(by).groupby(
                            by)[loadcol].nlargest(
                                npeaks, keep='first').reset_index()
    else:
        #Find the days with the largest MAX of hourly load
        daily_vals = data.groupby(
                        by+[daycol])[loadcol].max().reset_index(by).groupby(
                            by)[loadcol].nlargest(
                                npeaks, keep='first').reset_index()
    
    daily_vals['peakflag'] = 1
    
    #Return a series of flags indicating the peak days
    return data[by+[daycol]].merge(
        daily_vals, how='left', on=by+[daycol]).fillna(0)['peakflag']


def generate_day_types(data, **kwargs):
    '''
    Generate a column that codes days as either (1) peak, (2) weekday, or 
    (3) weekend, given an input data frame that contains calendar data 
    (from create_cal_data) and load data
    
    Arguments and keywords are identical to dates.get_peak_days()
    
    Returns the input data frame with a new 'Day Type' column.
    '''
    
    peakflag = get_peak_days(data, **kwargs)
    
    data['Day Type'] = 2
    data.loc[data['weekend']==1, 'Day Type'] = 3
    data.loc[peakflag==1, 'Day Type'] = 1
    
    return data