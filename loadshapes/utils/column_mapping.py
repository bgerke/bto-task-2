import pandas as pd

def map_columns(orig_df, col_map, map_to=1, map_from=0):
    '''
    Map a large number of end uses to a smaller number of categories, as 
    specified in a mapping dataframe.  The special string 'EXCLUDE' indicates
    that a particular column in the original dataframe should be excluded
    '''
    
    mapped_cols = col_map.loc[col_map.iloc[:,map_to]!='EXCLUDE'].iloc[:,map_to].unique()
    #Create a blank output df
    mapped_df = pd.DataFrame([[0]*len(orig_df)]*len(mapped_cols), 
                             index=mapped_cols).T
    
    for c in mapped_cols:
        orig_cols = col_map.loc[col_map.iloc[:,map_to]==c].iloc[:,map_from].values
        orig_cols = [col for col in orig_cols if (col in set(orig_df.columns))]

        mapped_df[c] += orig_df[orig_cols].sum(axis=1)

    
    return(mapped_df)
