
def extract_datetime_8760(df, hourfield = 'hour_ending', year = 2014):
    '''
    This function takes an 8760 and returns 
    the data frame with days and months attached
    
    INPUTS
    ------
    df -- an 8760 data set
    hourfiedl -- the field with each hour
    year -- the year of the 8760 data'''
    df['day_of_year'] = np.ceil(df[hourfield]/24).astype(int)
    dt = datetime.datetime(year,1,1,0,0)
    df['date'] = dt
    for ind in df.index:
        hours=df.loc[ind, hourfield]-1
        dtdelta = datetime.timedelta(hours = hours)
        df.loc[ind,'date'] = dt+dtdelta

    df['month'] = df['date'].dt.month
    
    return df